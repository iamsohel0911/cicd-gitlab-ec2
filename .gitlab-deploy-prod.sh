
set -f
string=$PRDO_DEPLOY_SERVER
array=(${string//,/ })

for i in "${!array[@]}"; do 
    echo "deploy project on server ${array[i]}"
    ssh ubuntu@${array[i]} "cd /var/www/cicd-gitlab-ec2 && git pull origin main && sudo pm2 start app.js"
done 
